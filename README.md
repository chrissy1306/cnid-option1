# CNID OPTION 1

## RUN

Have [vagrant](https://www.vagrantup.com/) installed

```
vagrant up
vagrant ssh
cd /vagrant
npm install
npm start
Go to http://192.168.1.120:3000/
```

## TESTS

```
npm test
```