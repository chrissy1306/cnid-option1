import React, { Component } from 'react';

class ArticleBody extends Component {
  getBody() {
    return this.props.body.map((item) => {
      switch (item.type) {
        case 'plaintext':
          return <p key={item.id}>{item.body}</p>;
        case 'h2':
          return <h2 key={item.id}>{item.body}</h2>;
        case 'pull_quote':
          return <blockquote key={item.id}>{item.body}</blockquote>;
        default:
          return '';
      }
    });
  }

  render() {
    return (
      <div className="article_body">
        { this.getBody() }
      </div>
    );
  }
}

export default ArticleBody;
