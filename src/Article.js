import React from 'react';
import { Link } from 'react-router-dom';

import ArticlesAPI from './api';
import ArticleBody from './ArticleBody';

const Article = (props) => {
  const article = ArticlesAPI.get(
    parseInt(props.match.params.id, 10),
  );

  if (!article) {
    return <div>Sorry, but the article was not found</div>;
  }

  return (
    <div className="article">
      <h1>{article.title}</h1>
      <ArticleBody body={article.body} />
      <Link to="/">Back</Link>
    </div>
  );
};

export default Article;
