import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Articles from './Articles';
import Article from './Article';

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Articles} />
      <Route exact path="/article/:id" component={Article} />
    </Switch>
  </BrowserRouter>,
  document.getElementById('root'),
);

