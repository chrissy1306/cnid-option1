import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import ArticlesAPI from './api';

class Articles extends Component {
  getArticles() {
    return ArticlesAPI.all().map(item => (
      <li key={item.id}>
        <Link to={`/article/${item.id}`}>
          <h2>{item.title}</h2>
          <img src={item.cover} alt={item.title} />
        </Link>
      </li>
    ));
  }

  render() {
    return (
      <ul>
        {this.getArticles()}
      </ul>
    );
  }
}

export default Articles;
