import React from "react";
import Articles from "../src/Articles.js"
import {shallow} from 'enzyme';

test('Articles: should display all the articles', () => {
  const wrapper = shallow(<Articles />);
  const articles = wrapper.find('li').length;
  expect(articles).toEqual(10);
})

test('Articles: should display title of article', () => {
  const wrapper = shallow(<Articles />);
  const headers = wrapper.find('h2');

  const expectedFirstTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry";
  const expectedSecondTitle = "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s";
  
  expect(headers.at(0).text()).toEqual(expectedFirstTitle);
  expect(headers.at(1).text()).toEqual(expectedSecondTitle);
})

test('Articles: should display img of article', () => {
  const wrapper = shallow(<Articles />);
  const imgs = wrapper.find('img');
  
  const expectedFirstImg = "https://s3-eu-west-1.amazonaws.com/cnid-tech-test/1.jpg";
  const expectedSecondImg = "https://s3-eu-west-1.amazonaws.com/cnid-tech-test/2.jpg";

  expect(imgs.at(0).prop('src')).toEqual(expectedFirstImg);
  expect(imgs.at(1).prop('src')).toEqual(expectedSecondImg);
})